-8<---------------cut here---------------start------------->8---
SLIDE 1: (16 secs)

Welcome to the second part of the Daily Use Tutorial for the Guix
package manager.  In this video we will explain more about the
concepts that characterise GNU Guix and show more of the commands to
use.  Please watch part one if you have not watched it already.  You
can find part one at
https://audio-video.gnu.org/guix/everyday-use-part1.webm.
----------------------------------------------------------------------
SLIDE 5: (30 secs)

Guix allows us to upgrade all the packages we have installed or to
upgrade just the packages we specify.  We can also control the package
versions. In the case where we want the latest version of a package it
is important to realise that the one that will be installed will be
the latest that is available in the current Guix version we have
installed on our system.  If we need to update our current Guix
version another command, 'guix pull', is needed.  As a consequence
both commands tend to be used together; first 'guix pull' and then
'guix package --upgrade'.
-----------------------------------------------------------------------
SLIDE 6: (40 secs)

Sometimes we need to free up extra disk space on our system.  This can
be achieved with two main commands.  Fist we run 'guix package
--delete-generations'.  Running this command without any other options
deletes all generations except for the current one.  In this case,
once the command is completed there is no previous generation to
rollback to.  We will see the effect of adding a pattern later.

Having run the previous 'guix package' command we can then call our
garbage collector, with 'guix gc'.  This command removes those
packages that are installed in the store but are no longer used.
-----------------------------------------------------------------------
CLI session 3:

First we will go back to take a look at our generations.  Now we will
delete only the first and third generations.  Then we check our
generations again.  Lastly we will delete all our generations except,
of course, the current one.  We check it.

And now we can run our garbage collector to free up disk space.  We
can see how much space we have freed.
-------------------------------------------------------------------
SLIDE 7: (5 secs)

And that's it. For further information please refer to the Guix
manual.
-8<---------------cut here---------------start------------->8---

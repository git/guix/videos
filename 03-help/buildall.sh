#!/bin/sh
# GNU Guix Video --- Scripts for videos presenting Guix
# Copyright © 2019 Laura Lazzati <laura.lazzati.15@gmail.com>
#
# This file is part of GNU Guix.
#
# GNU Guix is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# GNU Guix is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

# Script to build all video steps (CLI and non-CLI)
# and glue them together to the final video.

cd ..
make videoNoCli VIDEO=03-help LOCALE_LANG=en_US NUMBER=1
make videoNoCli VIDEO=03-help LOCALE_LANG=en_US NUMBER=2
make finalVideo VIDEO=03-help LOCALE_LANG=en_US


#! /bin/sh
# GNU Guix Video --- Scripts for videos presenting Guix
# Copyright © 2019 Laura Lazzati <laura.lazzati.15@gmail.com>
# Copyright © 2019 Björn Höfling <bjoern.hoefling@bjoernhoefling.de>
#
# This file is part of GNU Guix.
#
# GNU Guix is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# GNU Guix is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

#TODO: check order of variables, check that video name exists, check that locale is right, check that scriptFile exists, check that directories exist, 

errors()
{
	numberOfArgs=$1
	if [ $numberOfArgs -ne 4 ]
	then 
		usage
		exit 1
	fi
}

usage()
{
	echo "You need to pass arguments in the following order: video name, locale, session filename and number of video to create."
	echo "Example ./createCliVideo.sh  1-installationScript en_US firstCli 1" 
}

#variables
videoName=$1
locale=$2
sessionFile=$3
videoNumber=$4
sessionDirectory="${videoName}/${locale}/sessions/${sessionFile}"
outDirectory="${videoName}/${locale}/out/"

errors $#
rm -rf $outDirectory
mkdir $outDirectory
guile -e main -s ./screen $sessionDirectory $outDirectory
BACK_PID=$!
wait $BACK_PID
make videoCli VIDEO=${videoName} LOCALE_LANG=${locale} NUMBER=${videoNumber}
exit 0

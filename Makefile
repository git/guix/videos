# GNU Guix Video --- Scripts for videos presenting Guix
# Copyright © 2019 Laura Lazzati <laura.lazzati.15@gmail.com>
# Copyright © 2019 Björn Höfling <bjoern.hoefling@bjoernhoefling.de>
# Copyright © 2019 Paul Garlick <pgarlick@tourbillion-technology.com>
# Copyright © 2019 Ricardo Wurmus <rekado@elephly.net>
#
# This file is part of GNU Guix.
#
# GNU Guix is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# GNU Guix is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

########################## VARIABLES #################################################################
VIDEO=3-help
LOCALE_LANG=en_US
NUMBER=
TARGET=videoNoCli
SESSION=
SOUNDNAME=
SLIDES=

#make TARGET=videoCli NUMBER=2
# each video has its own list of slides
include $(VIDEO)/slidesList.mk

SLIDES:=$($(addsuffix $(NUMBER),SLIDES))
SESSION:=$($(addsuffix $(NUMBER),SESSION))
SOUNDNAME:=$($(addsuffix $(NUMBER),SOUNDNAME))
#default target: generate the video

#SVGS_TRANSLATION=$(addsuffix .svg, $(SLIDES)) 
SVGS:=$(addsuffix .svg, $(addprefix $(VIDEO)/en_US/svgs/, $(SLIDES)))
MP3S:=$(addsuffix .mp3, $(addprefix $(VIDEO)/$(LOCALE_LANG)/audios/, $(SLIDES)))
PNGS:=$(addsuffix .png, $(addprefix $(VIDEO)/$(LOCALE_LANG)/slides/, $(SLIDES)))

#for translations
POTS:= $(addsuffix .pot, $(SLIDES))
LOCALE_POTS_PATH:=$(addsuffix .pot, $(addprefix $(VIDEO)/$(LOCALE_LANG)/pots/, $(SLIDES)))
LOCALE_POS_PATH:= $(addsuffix .po, $(addprefix $(VIDEO)/en_US/translationFiles/, $(SLIDES)))
LOCALE_MOS_PATH:= $(addsuffix .mo, $(addprefix $(VIDEO)/en_US/translationFiles/, $(SLIDES)))

########################## ALL #############################################:##########################
.PHONY: all

all:$(TARGET)

######################## CREATE VIDEO NOCLI #########################################################

#make videoNoCli VIDEO=example LOCALE_LANG=en_US NUMBER=1

#Create all the .png files inside slides dir
$(VIDEO)/$(LOCALE_LANG)/slides/%.png: $(VIDEO)/$(LOCALE_LANG)/svgs/%.svg
	inkscape -z --export-png=$@ $<

#duration uses slides dir so it is OK, just moved svg files
%.duration: %.mp3
	duration=`ffprobe -v error -select_streams a:0 -show_entries stream=duration -of default=noprint_wrappers=1:nokey=1 $<`; \
	echo -e "file slides/$(notdir $(basename $<)).png\nduration $$duration" > $@;

#create durations.txt = it concatenates duration files that have each each slide in order and repeats the last two lines
$(VIDEO)/$(LOCALE_LANG)/durations.txt: $(MP3S:.mp3=.duration)  | $(MP3S) $(PNGS)
	cat $(sort $^) > $@ && tail -n 2 $@ >> $@

#create audio textfile and .mp3
$(VIDEO)/$(LOCALE_LANG)/audios/files.txt: $(MP3S)
	$(foreach O,$^,$(file >>$@,file $(notdir $O)))

$(VIDEO)/$(LOCALE_LANG)/audios/all.mp3: $(VIDEO)/$(LOCALE_LANG)/audios/files.txt
	ffmpeg -f concat -i $< -c:a copy $@

####Create final video ####
videoNoCli: $(NUMBER).clino.$(VIDEO).webm

$(NUMBER).clino.$(VIDEO).webm: \
	 $(VIDEO)/$(LOCALE_LANG)/durations.txt        \
	 $(VIDEO)/$(LOCALE_LANG)/audios/all.mp3	
		ffmpeg -y -f concat -vsync cfr \
		-i $(VIDEO)/$(LOCALE_LANG)/durations.txt \
		-i $(VIDEO)/$(LOCALE_LANG)/audios/all.mp3 \
		-c:a libopus -b:a 192k \
		-c:v libvpx-vp9 -crf 31 -b:v 0 -pix_fmt yuv420p \
		-af apad -shortest \
		-vf fps=25 -threads 4 $(VIDEO)/$(LOCALE_LANG)/videos/$@;
		make clean_noCli

############################ CREATE CLI VIDEO ########################################################

#make videoCli VIDEO=example LOCALE_LANG=en_US NUMBER=1

$(VIDEO)/$(LOCALE_LANG)/out/$(SESSION)-%.txt: $(VIDEO)/$(LOCALE_LANG)/sessions/$(SESSION)
	./createTxts.sh  $(VIDEO)/$(LOCALE_LANG)/sessions/$(SESSION)  $(VIDEO)/$(LOCALE_LANG)/out/

$(VIDEO)/$(LOCALE_LANG)/out/$(SESSION)-%.png: $(VIDEO)/$(LOCALE_LANG)/out/$(SESSION)-%.txt
	tail -n 20 $< | \
	paps --landscape --columns=80 --font "Monospace 20" | \
	convert -density 128 \
			-fill black \
			-background white \
			-flatten \
			-rotate 90 \
			-delete 0--2 - $@

videoCli: $(VIDEO)/$(LOCALE_LANG)/videos/$(NUMBER).cliyes.$(SESSION).webm

$(VIDEO)/$(LOCALE_LANG)/videos/$(NUMBER).cliyes.$(SESSION).webm: $(patsubst %.txt,%.png,$(wildcard $(VIDEO)/$(LOCALE_LANG)/out/$(SESSION)-*.txt))
ifeq ($(SOUNDNAME),)
	ffmpeg -framerate 25 -pattern_type glob -i '$(VIDEO)/$(LOCALE_LANG)/out/$(SESSION)-*.png' \
		-vf "scale='min(1920,iw)':min'(1080,ih)':force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2" \
		-c:v libvpx-vp9 -crf 31 -b:v 0 -pix_fmt yuv420p -threads 4 $@; 
else
	ffmpeg -y -framerate 25 -pattern_type glob -i '$(VIDEO)/$(LOCALE_LANG)/out/$(SESSION)-*.png' \
		-i $(VIDEO)/$(LOCALE_LANG)/audios/$(SOUNDNAME).mp3 \
		-vf "scale='min(1920,iw)':min'(1080,ih)':force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2" \
		-af apad -shortest \
		-c:a libopus -b:a 192k \
		-c:v libvpx-vp9 -crf 31 -b:v 0 -pix_fmt yuv420p -threads 4 $@; \
	rm $(VIDEO)/$(LOCALE_LANG)/out/$(SESSION)-*.png; \
	rm $(VIDEO)/$(LOCALE_LANG)/out/$(SESSION)-*.txt;


endif

########################### CONCATENATE TARGET ######################################################

finalVideo: $(VIDEO)/$(LOCALE_LANG)/videos/$(VIDEO).webm

$(VIDEO)/$(LOCALE_LANG)/videos/videos.txt: $(VIDEO)/$(LOCALE_LANG)/videos/*.webm
	$(foreach O,$(sort $^),$(file >>$@,file $(notdir $O)))

$(VIDEO)/$(LOCALE_LANG)/videos/video_streams.webm: $(VIDEO)/$(LOCALE_LANG)/videos/videos.txt
	ffmpeg -y -f concat -i $< -c:v copy -an $@

$(VIDEO)/$(LOCALE_LANG)/videos/audio_streams.opus: $(VIDEO)/$(LOCALE_LANG)/videos/videos.txt
	ffmpeg -y -f concat -i $< -c:a copy -vn $@

$(VIDEO)/$(LOCALE_LANG)/videos/$(VIDEO).webm: $(VIDEO)/$(LOCALE_LANG)/videos/video_streams.webm $(VIDEO)/$(LOCALE_LANG)/videos/audio_streams.opus
	ffmpeg -i $< -i $(word 2,$^) -c copy -map 0:v:0 -map 1:a:0 $@; \
	rm $(VIDEO)/$(LOCALE_LANG)/videos/videos.txt; \
	rm $(word 2,$^); \
	rm $<

finalVideoNoSound: $(VIDEO)/$(LOCALE_LANG)/videos/$(VIDEO).noSound.webm

$(VIDEO)/$(LOCALE_LANG)/videos/$(VIDEO).noSound.webm: $(VIDEO)/$(LOCALE_LANG)/videos/videos.txt
	ffmpeg -y -f concat -i $< -c copy -an  $@; \
		 rm $(VIDEO)/$(LOCALE_LANG)/videos/videos.txt


############################ FOR TRANSLATORS #########################################################

#create pots to send to translators only if LOCALE_LANG is en_US, otherwhise stop.
#  itstool -o de/1.pot 1.svg
.PHONY : pots_for_video
#make pots_for_video NUMBER=1
#TODO: SEE HOW ARE TREATED TRANSLATIONS OF CLI VIDEOS. THIS IS FOR NONCLI
pots_for_video: $(SVGS)
	$(foreach S,$^, itstool -o $(VIDEO)/en_US/pots/$(notdir $(basename $S)).pot $S;)

############################## TRANSLATION ###########################################################

.PHONY : slides

slides: pos mos translatedSVGS

pos: $(LOCALE_POTS_PATH)
	$(foreach P,$^, msginit --locale=$(LOCALE_LANG) --input=$P -o $(VIDEO)/en_US/translationFiles/$(notdir $(basename $P)).po;)

mos: $(LOCALE_POS_PATH)
	$(foreach P,$^,  msgfmt $P -o $(VIDEO)/en_US/translationFiles/$(notdir $(basename $P)).mo;)

translatedSVGS: $(LOCALE_MOS_PATH)
	$(foreach M,$^, itstool -l $(LOCALE_LANG) -m $M $(VIDEO)/en_US/svgs/$(notdir $(basename $M)).svg;) 
	$(foreach S,$^, mv $(notdir $(basename $S)).svg $(VIDEO)/$(LOCALE_LANG)/svgs;)		
 

############################### CLEAN ################################################################

.PHONY: clean_noCli clean_cli clean_all_videos clean_intermediate_videos clean_pots clean_locale_slides clean_locale_slides clean

clean:
	#videos
	make clean_noCli
	make 	clean_cli
	make clean_intermediate_videos
	make clean_finalVideo	
	#translations
	make clean_locale_slides
	make clean_pots

clean_noCli: 
ifeq ($(LOCALE_LANG), en_US)
	@echo "Removing en_US files"
else 
	@echo "Removing $(LOCALE_LANG) files"
endif
	rm -f $(VIDEO)/$(LOCALE_LANG)/audios/all.mp3
	rm -f $(VIDEO)/$(LOCALE_LANG)/audios/files.txt
	rm -f $(VIDEO)/$(LOCALE_LANG)/audios/*.duration
	rm -f $(VIDEO)/$(LOCALE_LANG)/slides/*.png
	rm -f $(VIDEO)/$(LOCALE_LANG)/durations.txt

clean_cli:
		rm -f $(VIDEO)/$(LOCALE_LANG)/out/*.txt; 
		rm -f $(VIDEO)/$(LOCALE_LANG)/out/*.png;

clean_intermediate_videos:
	rm -f $(VIDEO)/$(LOCALE_LANG)/videos/[0-9]*

clean_finalVideo:
	rm -f $(VIDEO)/$(LOCALE_LANG)/videos/$(VIDEO).webm
	rm -f $(VIDEO)/$(LOCALE_LANG)/videos/videos.txt

clean_pots:
	rm -f $(VIDEO)/en_US/pots/*.pot

clean_locale_slides:
	rm -f $(VIDEO)/en_US/translationFiles/*.po
	rm -f $(VIDEO)/en_US/translationFiles/*.mo
ifneq ($(LOCALE_LANG), en_US)
	rm -f $(VIDEO)/$(LOCALE_LANG)/svgs/*.svg
endif

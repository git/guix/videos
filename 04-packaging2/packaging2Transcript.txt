-8<---------------cut here--------------start------------->8---
SLIDE 1: (18 secs)

Welcome to the Packaging Tutorial for the Guix package manager.  In
this second video we will show you how to create a package from a
template.  Please recall watching the first video in this series in
which we learnt how to create a packaging environment.  In this
tutorial we are going to assume that you have already set up the
environment.
---------------------------------------------------------------------
SLIDE 2: (28 secs)

We are going to show you how to create an R programming language
package.  R packages are very useful for our community, particularly
for those who do statistical analysis.  The steps for creating our
package involve: First, getting into our already created environment.
Then finding an R package that is not already packaged.  After that
packaging it by downloading its template.  And lastly checking that it
works properly and then sending the new definition as a patch.
-----------------------------------------------------------------------
SLIDE 3:(21 secs)

We are going to package an R programming language package called aspi,
which can be found on the CRAN website.  CRAN is an online resource
for the R programming language and its packages.  Here you can see the
information provided by the website about the aspi package.
-----------------------------------------------------------------------
SLIDE 4: (13 secs)

This is our packaging algorithm.  First we have to choose what to
package.  Then we need to check whether it is already packaged.  If it
is we need to find another package.  If it isn't we can use a command
called 'guix import' which allows us to create a package definition,
or template, from a number of repositories.  One such repository is
CRAN.
--------------------------------------------------------------------------------
SLIDE 5:(10 secs)

Let's see some of this in action.
------------------------------------------------------------------------
CLI 1:

Our first step is checking if aspi is already packaged.  We will show
only the package names in our search since it is the name aspi we are
looking for.  The output says it is not present so we are going to see
how we can import the template.  Since we have a CRAN importer we can
just run the command adding the importer name and the package name.
The package name is looked up in the package description field of the
CRAN website.
-------------------------------------------------------------------------
SLIDE 6 (12 secs)

After getting our template there is a sequence of steps that we need
to follow.  First we create a new git branch so that we can leave our
master branch unchanged.  Then we append our new definition to the
CRAN scheme file.  We make some minor changes and lastly we test that
it builds correctly.
-------------------------------------------------------------------------
SLIDE 7(12 secs)

Here we are appending our package to the cran scheme file.  Notice
that we have only capitalized the first word of the synopsis and added
the plus sign at the end of the licence.
----------------------------------------------------------------------
SLIDE 8 (4 secs)

We also need to assert our Copyright at the top of the file.  We put
in the year in which we made the changes, our name and our email
address.
----------------------------------------------------------------------
SLIDE 9 (5 secs)

We will now see how to test that our package definition works!
------------------------------------------------------------------------
CLI 2:

The command that builds a package is called 'guix build'. Recall that
we are running everything in our build environment.  The output is
telling us that the package was built successfully. In the next video
we will see how to go on from here.
-------------------------------------------------------------------------
SLIDE 10 (5 secs)

And that's is. For further information please refer to the Guix
manual.
-8<---------------cut here---------------end------------->8---

